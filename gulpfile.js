const gulp        = require('gulp');
const browserSync = require('browser-sync').create();
const gulpSass    = require('gulp-sass');
const del         = require('del');
const exec        = require('child_process').exec;
const cleanCSS    = require('gulp-clean-css');
const rename      = require("gulp-rename");


function clean() {
    return del(['dist','styleguide/styles']);
}

// Compile sass into CSS & auto-inject into browsers
function sass() {
    return gulp.src("./scss/main.scss")
        .pipe(gulpSass({ includePaths: ['node_modules'] }).on('error', gulpSass.logError))
        .pipe(cleanCSS({debug: true}, (details) => {
            console.log(`${details.name}: ${details.stats.originalSize}`);
            console.log(`${details.name}: ${details.stats.minifiedSize}`);
        }))
        .pipe(gulp.dest("dist/css"))
        .pipe(browserSync.stream());
}

function buildStyleguide(cb) {
    exec('npm run compile-styleguide', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
}

function compileNucleusStyles() {
    return gulp.src("./styleguide/scss/app.scss")
        .pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(gulp.dest("./styleguide/styles"))
        .pipe(browserSync.stream());
}

function copyAssets() {
    return gulp.src('assets/**/*')
        .pipe(gulp.dest('dist/assets'));
}

function copyFontIcons() {
    return gulp.src('./node_modules/@fortawesome/fontawesome-free/webfonts/*')
        .pipe(gulp.dest('./dist/assets/icons'));
}

function copySass() {
    return gulp.src('./scss/**/*')
        .pipe(gulp.dest('./dist/scss'));
}

function copyPackage() {
    return gulp.src('./package-dist.json')
        .pipe(rename('package.json'))
        .pipe(gulp.dest('./dist'));
}

// Static Server + watching scss/html files
function serve() {

    browserSync.init({
        server: {
            baseDir: 'styleguide',
            routes: {
                "/dist": "dist",
                "/assets": "dist/assets"
            }
        }
    });

    gulp.watch("scss/**/*.scss", gulp.series(buildStyleguide, compileNucleusStyles, sass, copyFiles));
    gulp.watch("./styleguide/scss/**/*.scss", compileNucleusStyles);
    gulp.watch("dist/**/*.css").on('change', browserSync.reload);
    gulp.watch("./styleguide/styles/app.css").on('change', browserSync.reload);
    gulp.watch("./styleguide/theme/**/*.pug", gulp.series(buildStyleguide, compileNucleusStyles, browserSync.reload));
}

const copyFiles = gulp.series(copyAssets, copyFontIcons, copySass, copyPackage);
const dev = gulp.series(clean, copyFiles, sass, buildStyleguide, compileNucleusStyles, serve);
const build = gulp.series(clean, copyFiles, sass, copySass);

exports.dev = dev;
exports.default = dev;
exports.build = build;
exports.copyAssets = copyAssets;