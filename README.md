# Intro
> A living atomic styleguide for Endpoint web applications.
> A SCSS only framework with a living styleguide to be used in all Endpoint projects going forward.  

 

## Framework usage
To use the framework within your own application as a base for UI development.
( Will require your SSH access to the repository configred)

```sh
# NPM
npm install --save git+ssh://git@bitbucket.org:endpoint-esports/styleguide.git#master
# YARN
yarn add git+ssh://git@bitbucket.org:endpoint-esports/styleguide.git#master
```



### SCSS setup options
Once installed, you will need to pull in the framework into your applications SCSS and configure the asset path.

_the asset path MUST be before the import_

```scss
$pollen-assets-path: '~@endpoint/styleguide/assets';
@import '~@endpoint/styleguide/scss/main';
```

### Angular Material Support
The styleguide comes with a material theme ready to go and some overrides. In order to use the material theme in your
own product then you need to do the following (assuming angular material is already imported):

```scss
@import '~@angular/material/theming';
@import '~@endpoint/styleguide/scss/material/theme';
```



## Styleguide usage 
In order to see the styleguide, pull down this repository and run the dev command

```sh
git clone git@bitbucket.org:endpoint-esports/styleguide.git
cd styleguide
yarn install
yarn dev
```



## Development
The styleguide is generated from comments within the SCSS files themselves and is a fork of https://github.com/holidaypirates/nucleus

Run the style guide using the dev command as shown above. 
Edit the framework scss files under the ```scss``` directory, following the SCSS dev guidelines.
Gulp task should re-compile the SCSS on changes and reload BrowserSync for live updates.
*note: changes to example markup may require a manual browser refresh*

For documentation on the annotation and how to correctly comment the scss please refer to
[Annotation Documentation](https://holidaypirates.github.io/nucleus/annotation-reference.html)


## Adding icons to the font.
Add the .svg file to the ```assets/icons``` folder. Please rename to use hyphenated string rather than underscore.
Run ```npm run generate-icon-font```


## Release History
* 1.0.0 - initial release